/*
npm init -y
npm install --save-dev gulp
npm install gulp-pug --save-dev
npm install --save-dev browser-sync
npm install gulp-sass --save-dev
npm install --save-dev gulp-uglify
npm install gulp-cssnano --save-dev
npm install --save-dev gulp-imagemin
npm install --save-dev gulp-autoprefixer
npm i gulp-htmlmin --save-dev
*/
const gulp = require('gulp');
const  pug = require('gulp-pug');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const cssnano = require('gulp-cssnano');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const htmlmin = require('gulp-htmlmin');

gulp.task('default', ['sdev']);

/* gulp-pug */
gulp.task('devpug', () =>
  gulp.src('./0source/**/*.pug')
    .pipe(pug({pretty: true, basedir: '0source'}))
    //.pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./0developement/'))
);
gulp.task('propug', () =>
  gulp.src('./0source/**/*.pug')
    .pipe(pug({pretty: true, basedir: '0source'}))
    //.pipe(htmlmin({collapseWhitespace: true,removeComments: true, conservativeCollapse: true, }))
    .pipe(gulp.dest('./0PRODUCTION/'))
);
/* _gulp-pug_ */

/*  gulp-sass  */
gulp.task('devscss', function(){
	return gulp.src('./0source/**/*.scss')
		.pipe(sass())
		//.pipe(cssnano())
		.pipe(autoprefixer({
				browsers: ['last 10 versions'],
				cascade: false
		}))
		.pipe(gulp.dest('./0developement/'))
		.pipe(browserSync.stream());
});
gulp.task('devsass', function(){
	return gulp.src('./0source/**/*.sass')
		.pipe(sass())
		//.pipe(cssnano())
		.pipe(autoprefixer({
				browsers: ['last 10 versions'],
				cascade: false
		}))
		.pipe(gulp.dest('./0developement/'))
		.pipe(browserSync.stream());
});

gulp.task('proscss', function(){
	return gulp.src('./0source/**/*.scss')
		.pipe(sass())
		.pipe(cssnano())
		.pipe(autoprefixer({
				browsers: ['last 10 versions'],
				cascade: false
		}))
		.pipe(gulp.dest('./0PRODUCTION/'))
		.pipe(browserSync.stream());
});
gulp.task('prosass', function(){
	return gulp.src('./0source/**/*.sass')
		.pipe(sass())
		.pipe(cssnano())
		.pipe(autoprefixer({
				browsers: ['last 10 versions'],
				cascade: false
		}))
		.pipe(gulp.dest('./0PRODUCTION/'))
		.pipe(browserSync.stream());
});
		/* sass main*/
gulp.task('maindevsass', function(){
	return gulp.src('./0source/assets/main.sass')
		.pipe(sass())
		//.pipe(cssnano())
		.pipe(autoprefixer({
				browsers: ['last 10 versions'],
				cascade: false
		}))
		.pipe(gulp.dest('./0developement/assets/'))
		.pipe(browserSync.stream());
});
gulp.task('mainprosass', function(){
	return gulp.src('./0source/assets/main.sass')
		.pipe(sass())
		.pipe(cssnano())
		.pipe(autoprefixer({
				browsers: ['last 10 versions'],
				cascade: false
		}))
		.pipe(gulp.dest('./0PRODUCTION/assets/'))
		.pipe(browserSync.stream());
});
/*  _gulp-sass_  */

/* gulp-uglify */
gulp.task('devuglify', function(){
	gulp.src('./0source/**/*.js')
	.pipe(uglify())
	.pipe(gulp.dest('./0developement/'))
});
gulp.task('prouglify', function(){
	gulp.src('./0source/**/*.js')
	.pipe(uglify())
	.pipe(gulp.dest('./0PRODUCTION/'))
});
/* _gulp-uglify_ */

/* gulp-cssnano */
gulp.task('devcssnano', function() {
    return gulp.src('./0source/**/*.css')
        .pipe(cssnano())
        .pipe(gulp.dest('./0developement/'));
				// .pipe(autoprefixer({
				// 		browsers: ['last 10 versions'],
				// 		cascade: false
				// }));
				//.pipe(browserSync.stream());
});
gulp.task('procssnano', function() {
    return gulp.src('./0source/**/*.css')
        .pipe(cssnano())
        .pipe(gulp.dest('./0PRODUCTION/'));
});
/* _gulp-cssnano_ */

/* gulp-imagemin */
gulp.task('images', ['devimj','devimp','devimg','devims','proimj','proimp','proimg','proims']);

gulp.task('devimagemin', ['devimj','devimp','devimg','devims']);
gulp.task('devimj', () =>
    gulp.src('./0source/**/*.jpg')
        .pipe(imagemin())
        .pipe(gulp.dest('./0developement/'))
);
gulp.task('devimp', () =>
    gulp.src('./0source/**/*.png')
        .pipe(imagemin())
        .pipe(gulp.dest('./0developement/'))
);
gulp.task('devimg', () =>
    gulp.src('./0source/**/*.gif')
        .pipe(imagemin())
        .pipe(gulp.dest('./0developement/'))
);
gulp.task('devims', () =>
    gulp.src('./0source/**/*.svg')
        .pipe(imagemin())
        .pipe(gulp.dest('./0developement/'))
);
gulp.task('proimagemin', ['proimj','proimp','proimg','proims']);
gulp.task('proimj', () =>
    gulp.src('./0source/**/*.jpg')
        .pipe(imagemin())
        .pipe(gulp.dest('./0PRODUCTION/'))
);
gulp.task('proimp', () =>
    gulp.src('./0source/**/*.png')
        .pipe(imagemin())
        .pipe(gulp.dest('./0PRODUCTION/'))
);
gulp.task('proimg', () =>
    gulp.src('./0source/**/*.gif')
        .pipe(imagemin())
        .pipe(gulp.dest('./0PRODUCTION/'))
);
gulp.task('proims', () =>
    gulp.src('./0source/**/*.svg')
        .pipe(imagemin())
        .pipe(gulp.dest('./0PRODUCTION/'))
);
/* _gulp-imagemin_ */

/* gulp-autoprefixer */
gulp.task('devprefixer', () =>
    gulp.src('./0source/**/*.css')
        .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: true
        }))
        .pipe(gulp.dest('./0developement/'))
        //.pipe(browserSync.stream());
);
gulp.task('proprefixer', () =>
    gulp.src('./0source/**/*.css')
        .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
		.pipe(cssnano())
        .pipe(gulp.dest('./0PRODUCTION/'))
        //.pipe(browserSync.stream());
);
/* _gulp-autoprefixer_ */

/* gulp-htmlmin */
gulp.task('devhtmlmin', function() {
  return gulp.src('./0developement/**/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./0developement/'));
});
gulp.task('prohtmlmin', function() {
  return gulp.src('./0PRODUCTION/**/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./0PRODUCTION/'));
});
/* _gulp-htmlmin_ */

/* gulp-pug SERVERS */

gulp.task('sdev', ['devpug', 'maindevsass', 'devuglify'], function(){
  browserSync.init({
    server: "./0developement/"
  });
	gulp.watch('./0source/**/*.html', ['devpug']);
	gulp.watch('./0source/**/*.pug', ['devpug']);
	gulp.watch('./0source/**/*.jade', ['devpug']);
	gulp.watch('./0source/0pgs/assets/*.sass', ['maindevsass']);
	gulp.watch('./0source/0pgs/assets/*.scss', ['maindevsass']);
	gulp.watch('./0source/0pgs/assets/*.css', ['maindevsass']);
	gulp.watch('./0source/assets/main.sass', ['maindevsass']);
	gulp.watch('./0source/assets/**/*.js', ['devuglify']);
	gulp.watch('./0developement/**/*.*').on('change', browserSync.reload);
});

gulp.task('spro', ['propug','mainprosass', 'prouglify'], function(){
  browserSync.init({
    server: "./0PRODUCTION/"
  });
	gulp.watch('./0source/**/*.html', ['propug']);
	gulp.watch('./0source/**/*.pug', ['propug']);
	gulp.watch('./0source/**/*.jade', ['devpug']);
	gulp.watch('./0source/0pgs/assets/*.sass', ['mainprosass']);
	gulp.watch('./0source/0pgs/assets/*.scss', ['mainprosass']);
	gulp.watch('./0source/0pgs/assets/*.css', ['mainprosass']);
	gulp.watch('./0source/assets/main.sass', ['mainprosass']);
	gulp.watch('./0source/assets/**/*.js', ['prouglify']);
	gulp.watch('./0PRODUCTION/**/*.*').on('change', browserSync.reload);
});


gulp.task('justsdev', function(){
  browserSync.init({
    server: "./0developement/"
  });
	gulp.watch('./0source/**/*.html', ['devpug']);
	gulp.watch('./0source/**/*.pug', ['devpug']);
	gulp.watch('./0source/**/*.jade', ['devpug']);
	gulp.watch('./0source/0pgs/assets/*.sass', ['maindevsass']);
	gulp.watch('./0source/0pgs/assets/*.scss', ['maindevsass']);
	gulp.watch('./0source/0pgs/assets/*.css', ['maindevsass']);
	gulp.watch('./0source/assets/main.sass', ['maindevsass']);
	gulp.watch('./0source/assets/**/*.js', ['devuglify']);
	gulp.watch('./0developement/**/*.*').on('change', browserSync.reload);
});

gulp.task('justspro', function(){
  browserSync.init({
    server: "./0PRODUCTION/"
  });
	gulp.watch('./0source/**/*.html', ['propug']);
	gulp.watch('./0source/**/*.pug', ['propug']);
	gulp.watch('./0source/**/*.jade', ['devpug']);
	gulp.watch('./0source/0pgs/assets/*.sass', ['mainprosass']);
	gulp.watch('./0source/0pgs/assets/*.scss', ['mainprosass']);
	gulp.watch('./0source/0pgs/assets/*.css', ['mainprosass']);
	gulp.watch('./0source/assets/main.sass', ['mainprosass']);
	gulp.watch('./0source/assets/**/*.js', ['prouglify']);
	//gulp.watch('./0PRODUCTION/**/*.html', ['prohtmlmin']);
	gulp.watch('./0PRODUCTION/**/*.*').on('change', browserSync.reload);
});
/* _gulp-pug_ _SERVERS_ */
